-- Septian Razi u6086829
-- COMP1130 Assignment 2 (2017)
-- Australian National University

--------------------------------- Burrows Wheeler Transform Compression -------------------------------
-- Includes:
--functions to encode list into a burrows wheeler transform list
-- functions to decode a burrows wheeler transform list into a list
---------------------------------------------------------------------------------------------------------


module BWT where

import Data.Binary
import Data.List

import RLE

data BWTValue a
  = Value a -- Wraps a character from the original string.
  | EOL -- End of line
  deriving (Ord, Eq)

-- Transform a string into a string with the `special end character'
mark_end :: [a] -> [BWTValue a]
mark_end l = helper l []
    where
    helper [] acc = reverse (EOL:acc)
    helper (x:xs) acc = helper (xs) ((Value x):acc)

-- Produce all rotations of the input string.
-- Hint: look at the functions `inits` and `tails` and zipWith
make_rotations :: [BWTValue a] -> [[BWTValue a]]
make_rotations l =  deleteFirst (zipWith (++) (tails l) (inits l))
    where
    deleteFirst (x:xs) = xs

-- Sort the list of rotations, using prelude function sort to do sorting
sort_rotations
  :: Ord a
  => [[BWTValue a]] -> [[BWTValue a]]
sort_rotations l =  sort l


-- Retrive the last BWTValue a from each of the sorted rotations
get_lasts :: [[BWTValue a]] -> [BWTValue a]
get_lasts l = map last l

-- Join it all together
bwt_encode
  :: Ord a
  => [a] -> [BWTValue a]
bwt_encode l = encoded
    where
    message = mark_end l
    rotations = make_rotations message
    sortedRotations = sort_rotations rotations
    encoded = get_lasts sortedRotations

-- takes BWT encoded string and it's length and output all possible rotations of string
-- assumes length is true for input BWT encoded string
make_bwt_table
  :: Ord a
  => Int -> [BWTValue a] -> [[BWTValue a]]
make_bwt_table n l = helper n l (map (:[]) l)
    where
    helper 1 ori (x:xs) = sort (x:xs)
    helper n ori (x:xs) = helper (n-1) ori (zipWith (:) ori (sort (x:xs)))

-- assumes first value is EOL from find_EOL_line
remove_BWTValues :: [BWTValue a] -> [a]
remove_BWTValues (x:xs) = map removeBWTValue xs
    where
    removeBWTValue a = case a of
        Value x -> x
        EOL -> error "More than one EOL line is found in input"

--finds the BWTValue with the correct order
-- Correct order of BWTValue list is the one with the EOL element at the front/back
-- Chose to use front EOL parameter for efficiency
find_EOL_line :: [[BWTValue a]] -> [BWTValue a]
find_EOL_line [] = error "cannot decode message as no EOL line is found"
find_EOL_line ((b:bs):xs) = case b of
    EOL     -> (b:bs)
    _       -> find_EOL_line xs

-- Decode BWT
bwt_decode
  :: Ord a
  => [BWTValue a] -> [a]
bwt_decode l = message
    where
    message = remove_BWTValues correctOrder
    correctOrder = find_EOL_line table
    table = make_bwt_table (length l) l

instance Show a =>
         Show (BWTValue a) where
  show EOL = "@"
  show (Value x) = show x

instance Binary a =>
         Binary (BWTValue a) where
  put EOL = putWord8 0
  put (Value x) = putWord8 1 >> put x
  get = do
    n <- getWord8
    case n of
      0 -> return EOL
      1 -> fmap Value get
      _ -> error $ "get(BWTValue a): unexpected tag: " ++ show n

instance Functor BWTValue where
  fmap _ EOL = EOL
  fmap f (Value x) = Value (f x)


-----------------------------------------------------------------------------------------------------
---TESTING PROPERTIES----------------------------------------

prop_mark_end :: Bool
prop_mark_end = mark_end "^BANANA" == [Value '^',Value 'B',Value 'A',Value 'N',Value 'A',Value 'N',Value 'A',EOL]

prop_make_rotations :: Bool
prop_make_rotations = make_rotations (mark_end "123") ==
                       [[Value '2',Value '3',EOL,Value '1'],[Value '3',EOL,Value '1',Value '2'],[EOL,Value '1',Value '2',Value '3'],[Value '1',Value '2',Value '3',EOL]]

prop_sort_rotations :: Bool
prop_sort_rotations = sort_rotations (make_rotations (mark_end "123")) ==
                       [[Value '1',Value '2',Value '3',EOL],[Value '2',Value '3',EOL,Value '1'],[Value '3',EOL,Value '1',Value '2'],[EOL,Value '1',Value '2',Value '3']]

prop_bwt_encode :: Bool
prop_bwt_encode = (bwt_encode ("^BANANA")) == [Value 'B',Value 'N',Value 'N',Value '^',Value 'A',Value 'A',EOL,Value 'A']

prop_bwt_decode :: Bool
prop_bwt_decode = (bwt_decode [Value 'B',Value 'N',Value 'N',Value '^',Value 'A',Value 'A',EOL,Value 'A']) == ("^BANANA")

prop_BWT_lossless :: Ord a => [a] -> Bool
prop_BWT_lossless l = bwt_decode (bwt_encode l) == l