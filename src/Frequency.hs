{-# LANGUAGE BangPatterns #-}

-------------------------------------------------------------------------
--                              
--         Frequency.hs
--                              
--         Calculate the frequencies of words in a text, used in
--         Huffman coding.
-------------------------------------------------------------------------

-- Septian Razi u6086829
-- COMP1130 Assignment 2 (2017)
-- Australian National University

module Frequency
  ( frequency
  ) where

import Prelude hiding (lookup)
import Data.Map (Map, lookup, insert, empty, assocs)

-- A frequency histogram is a map from values to `Int`s
type Histogram a = Map a Int


-- | inc
-- Increment the value associated with the given key in the input histogram.
-- WARNING: Do not change this function!
-- Example:
-- >>> inc empty 'a'
-- fromList [('a',1)]
inc
  :: Ord a
  => Histogram a -> a -> Histogram a
inc !h x =
  case lookup x h of
    Nothing -> insert x 1 h
    Just !n -> insert x (n + 1) h

-- Calculate the frequencies of characters in a list.
frequency
  :: Ord a
  => [a] -> [(a, Int)]
frequency xs = assocs (create empty xs)

-- | create
-- Create a new histogram that augments the input histogram with
-- the keys given in the input list.
--
-- Example:
-- >>> create empty "abacab"
-- fromList [('a',3),('b',2),('c',1)]
create :: Ord a => Histogram a -> [a] -> Histogram a
create hist ([]) = hist
create hist (x:xs) = create (inc hist x) (xs)


-----------------------------------------------------------------------------------------------------------------------------
----------------TESTING-------------------------------------------------------------------------------------------------

prop_frequency :: Bool
prop_frequency = (frequency "memes") == [('e',2),('m',2),('s',1)] &&
                    (frequency "abcdefghijklmnopqrstuvwxyz ") == [(' ',1),('a',1),('b',1),('c',1),('d',1),('e',1),('f',1),('g',1),('h',1),('i',1),('j',1),('k',1),('l',1),('m',1),('n',1),('o',1),('p',1),('q',1),('r',1),('s',1),('t',1),('u',1),('v',1),('w',1),('x',1),('y',1),('z',1)] &&
                    (frequency "^BANANA") == [('A',3),('B',1),('N',2),('^',1)]



