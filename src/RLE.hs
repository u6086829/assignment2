-- Septian Razi u6086829
-- COMP1130 Assignment 2 (2017)
-- Australian National University

--------------------------------- Run-Length Encoding Compression -------------------------------
-- Includes:
-- functions to encode list into a run-length key-value pair list
-- functions to decode a run-length key-value pair list into a list
---------------------------------------------------------------------------------------------------------

module RLE where

import Data.List

rle_encode :: Eq a => [a] -> [(a,Int)]
rle_encode [] = []
rle_encode a = helper a []
    where
    helper [] c =  foldKP c []
    helper (x:xs) c = helper (xs) ((x,1):c)

--merges tuples in a list and append the new tuples in a new list
--used in rle_encode to merge duplicate tuples into one large tuples
foldKP :: Eq a => [(a,Int)] -> [(a,Int)] -> [(a,Int)]
foldKP l acc = helper l acc 254
    where
    helper [] acc n = acc
    helper ((a,an):(b,bn):xs) acc n
        | a == b && n > 0 = helper ((a,(an+bn)):xs) acc (n-1)
        | otherwise = helper ((b,bn):xs) ((a,an):acc) 254
    helper [x] acc n = x:acc


rle_decode :: [(a,Int)] -> [a]
rle_decode l = helper l []
    where
    helper [] acc = reverse acc
    helper ((x,n):xs) acc = helper xs (appendFor x n acc)

-- function to append an element a given number of times
-- used in rle_decode
-- appendFor 'x' 4 ['b']
-- >>> "xxxxb"
appendFor :: a -> Int -> [a] -> [a]
appendFor a 0 list = list
appendFor a n list = appendFor a (n-1) (a:list)

-------------------------------------------------------------------------------------------------------------------------------
--- Tests for encode and decode
-------------------------------------------------------------------------------------------------------------------------------
prop_rle_encode :: Bool
prop_rle_encode = (rle_encode ("aaaabaaab")) == [('a',4),('b',1),('a',3),('b',1)]

prop_rle_decode :: Bool
prop_rle_decode = (rle_decode [('a',4),('b',1),('a',3),('b',1)]) == ("aaaabaaab")

prop_RLE_lossless :: Eq a => [a] -> Bool
prop_RLE_lossless l = (rle_decode(rle_encode l) == l)
