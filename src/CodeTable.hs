-------------------------------------------------------------------------
--
--         CodeTable.hs
--
--         Converting a Huffman tree to a table.
--
-------------------------------------------------------------------------

-- Septian Razi u6086829
-- COMP1130 Assignment 2 (2017)
-- Australian National University

module CodeTable
  ( codeTable,
  testTree,
  testTree2, convert
  ) where

import Types (Tree(Leaf, Node), Bit(L, R), HCode, Table)
import Data.Map (fromList, assocs)

-- Making a table from a Huffman tree.
codeTable :: Ord a => Tree a -> Table a
codeTable t = fromList (convert [] t)

-- Auxiliary function used in conversion to a table. The first argument is
-- the HCode which codes the path in the tree to the current Node, and so
-- codeTable is initialised with an empty such sequence.
convert :: Ord a => HCode -> Tree a -> [(a,HCode)]
convert hC (Leaf n a) = [(a,hC)]    -- if given tree is simply a leaf, return huffman key pair unaltered
convert hC t = mapHCode (hC++) (mapHCode reverse (helper [] t)) -- append prefix HCode to reversed HCode from helper
   where
   helper c (Leaf n a) = [(a,c)]
   helper c (Node n l r) = (helper (L:c) l) ++ (helper (R:c) r)

-- function to map a function to only the HCode in a key-HCode pair
-- used in convert function
mapHCode :: (HCode -> HCode) -> [(a,HCode)] -> [(a,HCode)]
mapHCode fx [] = []
mapHCode fx ((a,c):xs) = ((a,(fx c)):(mapHCode fx xs))




--functions and sample trees test out convert function---------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------
testTree = Node 20 (Node 15 (Leaf 10 'a') (Leaf 5 'b')) (Node 5 (Leaf 3 'c') (Leaf 2 'd'))

testTree2 = Node 62
                (Leaf 24 'a')
                (Node 38
                    (Node 22
                        (Leaf 12 'b')
                        (Leaf 10 'c'))
                    (Node 16
                        (Leaf 8 'd')
                        (Leaf 8 'e')))

convertTest :: Bool
convertTest =   convert [] testTree == [('a',[L,L]),('b',[L,R]),('c',[R,L]),('d',[R,R])] &&
                convert [R,L,R,L] testTree2 == [('a',[R,L,R,L,L]),('b',[R,L,R,L,R,L,L]),('c',[R,L,R,L,R,L,R]),
                                                ('d',[R,L,R,L,R,R,L]),('e',[R,L,R,L,R,R,R])] &&
                convert [L,R] (Leaf 1 'a') == [('a',[L,R])]

