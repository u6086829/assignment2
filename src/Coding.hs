-------------------------------------------------------------------------
--  
--         Coding.hs                            
--                              
--         Huffman coding in Haskell.                   
--
--         The top-level functions for coding and decoding.
--
-------------------------------------------------------------------------

-- Septian Razi u6086829
-- COMP1130 Assignment 2 (2017)
-- Australian National University

module Coding
  ( encodeMessage
  , decodeMessage
  ) where

import Types (Tree(Leaf, Node), Bit(L, R), HCode, Table)

import Prelude hiding (lookup)
import qualified Data.Map as Map
import Data.Maybe (fromMaybe)
{-# ANN module ("HLint: ignore Eta reduce"::String) #-}

-- Code a message according to a table of codes.            
encodeMessage :: Ord a => Table a -> [a] -> HCode
encodeMessage tbl el = helper el tbl []
    where
    helper [] tbl hc = hc
    helper (x:xs) tbl hc = helper xs tbl (hc ++ (lookup tbl x))  -- finds hc value for x element and appends it to HCode



-- lookup looks up the meaning of an individual char in a Table.
lookup :: Ord a => Table a -> a -> HCode
lookup m c = fromMaybe (error "lookup") (Map.lookup c m)



-- Decode a message according to a tree.
--
-- The first tree argument is constant, being the tree of codes;
-- the second represents the current position in the tree relative
-- to the (partial) HCode read so far.
decodeMessage :: Ord a => Tree a -> HCode -> [a]
decodeMessage t hC = reverse (firstHelper t hC [])
    where
    firstHelper t [] msg = msg                  -- if huffman code is empty, message has been decoded
    firstHelper t hC msg = helper t t hC msg    -- sends 2 instances of t, one for original unaltered t and one for the helper to traverse deeper into tree
        where
        helper iniT (Leaf n a) hC msg = firstHelper iniT hC (a:msg)
        helper iniT (Node n l r) (x:xs) msg= case x of
            L   -> helper iniT l (xs) msg
            R   -> helper iniT r (xs) msg
        helper iniT (Node n l r) [] msg = firstHelper iniT [] (msg) -- if hC is empty at a node, simply just return the available message

