-- Septian Razi u6086829
-- COMP1130 Assignment 2 (2017)
-- Australian National University

--------------------------------- Move-to-Front Transform Compression -------------------------------
-- Includes:
-- functions to encode list into move-to-front list of digits
-- functions to decode a move-to-front list of digits into a list
---------------------------------------------------------------------------------------------------------

module MTF where

import Data.List (elemIndex, delete)


mtf_encode :: Eq a => [a] -> [a] -> [Int]
mtf_encode a b = helper a b []
    where
    helper l [] acc = reverse acc
    helper l (x:xs) acc = helper (moveToFront l x) xs ((findIndex l x):acc)

mtf_decode :: Eq a => [a] -> [Int] -> [a]
mtf_decode a c = helper a c []
    where
    helper l [] acc = reverse acc
    helper l (n:ns) acc = helper (moveToFront l dec) ns (dec:acc)
        where
        (Just dec) = (safeIndex n l)

safeIndex :: Int -> [a] -> Maybe a
safeIndex n _      | n < 0 = Nothing
safeIndex 0 (x:xs) = Just x
safeIndex n (_:xs) = safeIndex (n-1) xs
safeIndex _ []     = Nothing

-- takes a list and an element and moves the corresponding element in the given list to the front
--assumes input list has no repeat elements
moveToFront :: Eq a => [a] -> a -> [a]
moveToFront (x:xs) a = a:fl ++ (tail el)
    where
    (fl,el) = splitAt (findIndex (x:xs) a) (x:xs)

--assumes element always has a member in list
findIndex :: Eq a => [a] -> a -> Int
findIndex a n = ind
    where
    (Just ind) = elemIndex n a


----TESTING AND PROPS---------------------------------------------------
alphabet :: String
alphabet = "abcdefghijklmnopqrstuvwxyz "

prop_mtf_encode :: Bool
prop_mtf_encode = (mtf_encode alphabet "bananaaa") == [1,1,13,1,1,1,0,0]

prop_MTF_lossless :: [Char] -> Bool
prop_MTF_lossless l = (mtf_decode alphabet (mtf_encode alphabet l)) == l